This is a backup of notes, taken after call ended. Will be updated after every call. Original etherpad can be found [here](https://etherpad.net/p/GOSHcall)

Related to issue #110

***
# GOSH community call #7
- Time/Date: December, 3rd - 19h UTC 
- How to join: https://zoom.us/j/672578055
- Link to these notes: https://etherpad.net/p/GOSHcall

## ROLL CALL: write your name / where are you / link to project-profile-social media
- Julieta / Geneva, CH / GH: @thessaly TW: @cassandreces
- Fernan/Chile
- Alejo
- Saulo / Barcelona, SPA
- Ryan / Canada / @ryanfobel
- Valentina
- Xose y Arturo (IMVEC.tech)
-Solomon Kembo (University of Zimbabwe)

## Hacking ecology - Saulo Jacques:
Saulo - ecologist, impact of climate change in microbial communities in water systems
PhD Brasil / British Columbia

Problems: 
- lack of water quality data available, esp Global South 
- lots of sensors can connect to arduino or similar, and although they may be well documented they're indeed closed/black box
- centralization of data, how to deal with all the generated data

Project started as workshops and classes, with some activities developed by member of CTA-Brasil    

Goal: real open sensors + security, trust, scalable

- ph
- DO
- ORP
- Conductivity 
- total solids
- Salinity
- gps
- Temperature

modular

Moved to [Apache Kafka](https://kafka.apache.org/) from InfluxDB - better option

Used by Ecology Lab, Public Health Lab Institute. Aiming to work with citizen science projects.

cost 800EUR but modular, using sensors that are tested and reliable. 

Part of Lana - citizen science group

Check out [FieldKit](https://hackaday.io/project/26354-fieldkit)

Cosensores: http://cosensores.qb.fcen.uba.ar/

Presentation link: https://hackingecology.com/wp-content/uploads/2019/12/Community-Call_Dec_2019.pdf

***
# GOSH community call 6
- Time/Date: November, 12th - 16h UTC 
- How to join: https://zoom.us/j/672578055
- Link to these notes: https://etherpad.net/p/GOSHcall

## ROLL CALL: write your name / where are you / link to project-profile-social media
- Liz Barry, PublicLab.org
- Harold Tay
- Julieta Arancio
- Jeremy Bonvoisin
- Urs Gaudenz
- Marc Dusseiller

## Open Know-how specification (Jeremy Bonvoisin - Univ of Bath)
https://board.net/p/GOSHCall-2019-11-12-OpenKnowHow

Initiated by Andrew Lamb, Suttleworth fellow and member of FieldReady

Docs to facilitate
- Discoverability
- Portability
- Interoperability

Many actors from OSH involved, appropedia, fablabs, gosh, oshwa

Machine-readable data to be found by crawlers in a "manifest" file. 

Bigger project observatory https://oho.wiki/

DIN spec is CC-licensed

Julietta: at wikimedia Germany, they met people who offer professional services to improving your wiki to include a standards layer. 

Liz: could have implications for the wiki publiclab.org // Talk to Emilio from Appropedia, they also want to work on structuring data in wikis

Jeremy: See https://www.mediawiki.org/wiki/Wikibase

***
# GOSH community call 5

- Time/Date: October, 4th - 14h UTC 
- How to join: https://zoom.us/j/672578055
- Link to these notes: https://etherpad.net/p/GOSHcall

**ROLL CALL: write your name / where are you / link to project-profile-social media**
- Andre Chagas
- Jose Urra
- Julieta Arancio

- Andre: today it was released an open know how standard that can help making open science hardware more findable https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/wiki/Home
- Jose also working in a similar project - 

## Mozilla open hardware mentorship program

Link to Jose's presentation: https://docs.google.com/presentation/d/1XlRSxUUv56bv9cZeb7dTvsOUTUpclyUyyJecbsSDmiY/edit#slide=id.p

- Purpose is to empower open hw project leads to build better projects and communities by following best practices
- There is usually a roadmap for developing software projects, but there is none for hardware.
- Audience is people developing or wanting to develop an open hw project, or interested in growing community/onboarding contributors
- Mentees are domain experts that started working or are interested in open hw but need some orientation, not tech experts
- Idea is to learn from Mozilla Open Leaders lessons but make it useful for a hardware context
- Tools are taken from a diversity of approaches: user and domain centered design, technical centered design, community centered design, valorization strategies
- Findability should be a part of the program

Some topics in the program include:
- How to grow a community that helps sustaining the project?
- Open hardware business models
- Validating and testing of assumptions

- Program is not centered in which technology people use, but strategies to implement
- Participants receive 1:1 mentorship and support in growing their community
- Participants can return to the program next cohort as a mentor, after they've received mentorship. 
- Community can engage as experts

## Open discussion
How to decentralize a program like this, sustain it in time, scale in a decentralized way?

## Questions
- how much can be covered in the length of the program? Make references to material, things to take into account, not necessary to be professional
- how to address diversity of projects? give tools to build their path, help them ask themselves questions 
- people start from different points in the process of development, so will align for tailored goals
- a good expert is not always a good mentor, domain-experts can also engage as potential users
- people should find domain expertise

Call recording will be available at the community calls page: http://openhardware.science/about/community-calls/

***
# GOSH community call 4

- Time/Date: August, 27th - 10am UTC 
- How to join: https://zoom.us/j/672578055
- Link to these notes: https://etherpad.net/p/GOSHcall

**ROLL CALL: write your name / where are you / link to project-profile-social media**
- Julieta Arancio / CENIT UNSAM / GH @thessaly TW @cassandreces
- Victor  Kumbol /DIY Labware/ @vkumbol @dlabware
- Andre M Chagas / Uni Sussex, Trend in Africa, Open Neuroscience @chagas_am
- Daniel Wessolek / Berlin / @wessolek 
- Harold Tay / Singapore / htarold@gmail.com

## Introductions
Nielek : https://matchmymaker.de/ ; https://be-able.info/en/projekte/HACKademy/

## Victor Kumbol - DIY Labware Ghana
- Opportunity for remote mentoring (gosh community mentors?)
- Kumasi Hive there are people interested, for developing a local tech spport/together with students (clubs)
- lack of institutional support is more apathy than resistance, not high quality equipment

Call recording will be available at the community calls page: http://openhardware.science/about/community-calls/

***
# GOSH community call 3

- Time/Date: June, 4th - 9am EST (check timezone here shorturl.at/jmnEH )
- How to join: https://meet.kitspace.org/gosh 
- Link to these notes: https://etherpad.net/p/GOSHcall

**ROLL CALL: write your name / where are you / link to project-profile-social media**
- Richard Bowman / University of Bath
- Julian Stirling / Uni of Bath / https://gitlab.com/julianstirling
- Julieta Arancio / CENIT-UNSAM / gh: thessaly tw: cassandreces
- Greg Austic
- Oliver Keller / CERN&Uni Geneva / gh&tw: 0zelot
- Paz Bernaldo
- Liz Barry

## Recap activities post GOSH 2018
**How do we get people to participate more in the gitlab issues/activities post gosh 2018?**

- Get everyone to sign up to GitLab at GOSH so they can be part of issue tracking
- Mid-term post on status of issues
- Do we update who is a nudger?
- labels for 'merged' 'successful' etc (https://gitlab.com/gosh-community/gosh-roadmap/issues/165)
- milestones - (EPICS? this would be premium, how much does that cost!?)

**How do we decide an acceptable level of 'failure'?**    
**How do we can make sure everyone can find regional events (AfricaOSH is not on openhardware.science)**

## Community call
How do we keep these calls going? It is a lot of work to get peopl to present/talk.

Automate calendard/time https://gitlab.com/gosh-community/gosh-roadmap/issues/166

Have calls in different languages - or extra calls in another language

Use forum to suggest topics

If not enough topics are suggested use most comon formum posts

Plan presnetations well in advance!!! A years schedule on the forum?   (https://gitlab.com/gosh-community/gosh-roadmap/issues/166)

How do we make these less painful. "Can you hear me!?!?" - use zoom?

## CCC (Chaos Computer Club)
Good interest during last event (Oliver + Kaspar)

Had and OpenFlexure and a FlyPi

Posted summary in the forum (https://forum.openhardware.science/t/open-science-hardware-assembly-at-35c3/1556/9)

## GOSH Great Lakes
Toronto, July 31 - August 2, 2019 

% of new people

lessons from AfricaOSH?
    
***
# GOSH community call 2

- Time/Date: April, 11th - 4pm CEST (check timezone here https://shorturl.at/enovZ )
- How to join: https://meet.kitspace.org/gosh 
- Link to these notes: https://etherpad.net/p/GOSHcall

## ROLL CALL: write your name / where are you / link to project-profile / social media

- Julieta / Geneva, CH / https://github.com/thessaly / tw: @cassandreces /  @cassandreces@scholar.social
- Kaspar / Bristol UK / https://monostable.co.uk / @kaspar_e
- Kshitiz / Kathmandu / @kshitizkhanal7 / http://oknp.org
- Harold Tay / Singapore / htarold@gmail.com
- Ryan Fobel / Kitchener, Canada / https://sci-bots.com / @ryanfobel
- Gaston Corthey / San Martin, Buenos Aires, Argentina / UNSAM and TECSCI / @gcorthey / tw: @gcorthey
- Gustavo Pereyra Irujo / Balcarce, Buenos Aires, Argentina / http://vuela.cc/ / tw: @gpereyrairujo
- Liz Barry / Brooklyn, NYC, USA / https://publiclab.org / tw: @lizbarry

## OPENING QUESTION: if you had the money&time, which open hw project you'd build?

- Aerogami https://github.com/kshitizkhanal7/Aerogami
- Solar heater: https://gitlab.com/ryanfobel/open-solar-furnace

## AFRICA OSH NEWS

- Social media #AfricaOSH19
- http://africaosh.com/

## ReSeq Project: 

- https://wemakeit.com/projects/reseq-reuse-dna-sequencers
- https://www.hackteria.org/wiki/HiSeq2000_-_Next_Level_Hacking

Machines are 7 years old, there are getting obsolete and replaced for more efficient ones, so these are being decommissioned, sold by parts or very cheap

Can be reverse engineered and get the designs, would take looong time

Aim to make it available with FOSS and create customized devices

## Vuela project (http://vuela.cc/)
Paz & Gustavo work with communities and researchers in Chile, Argentina, Brazil, Uruguay and Paraguay, on open source drones that can be used for community and academic science

- Made +40 workshops, +100 people
- They aim to work with people from different backgrounds, this is challenging
- Mix people: academia/non academia how to mix them in the same room??? Scientists attend some, community attends others
- Open hardware is supposed to help people from different backgrounds work together, but it is not enough. 
- Differences between being paid or not to do it, having a clear aim in mind

Project background: https://medium.com/@PazByC/chronicle-of-a-flight-foretold-6253cc9291ae

Blog post about challenges and learnings: https://medium.com/@PazByC/chronicle-of-a-flight-foretold-6253cc9291ae 

TL;DR in twitter: https://twitter.com/PazByC/status/1112786342956527622

## Open educational resources (Kshitiz)

Teaching with very simple devices and experiments for students, started in 2016 (https://github.com/kshitizkhanal7/Aerogami)

Link to info about gitlab issue: https://forum.openhardware.science/t/moving-forward-from-gitlab-issues-immerse-students-in-making-hacking-non-traditional-learning-environment-94/1765

***
# GOSH community call 1

- Time/Date: March, 7th - 12h (CET), monthly
- How to join: https://jitsi.riot.im/GOSHcall
- Link to these notes: https://etherpad.net/p/GOSHcall

## ROLL CALL: write your name / location / link to project-profile / social media

- Julieta / Geneva, Switzerland / https://github.com/rlyehlab/eter / @cassandreces@scholar.social

- Andre / Brighton (University of Sussex) / https://fosh-following-demand.github.io/en/home /@chagas_am (twitter)

- Julian & Richard / University of Bath, UK / https://gitlab.com/bath_open_instrumentation_group/ @mindnumbed #openflexure

- Nano / Mendoza, Argentina / https://gitlab.com/nanocastro?nav_source=navbar

- Tobey (EMBL, Germany, @MakerTobey on Twitter), #DocuBricks, Journal of Opeen Hardware @journalopenhw)

- Harry Akligoh/Ghana/AfricaOSH/@harryakligoh 

- Kwabena Kesse /Ghana

- Jo Havemann, Africarxiv.org/AfricaOSH, @johave @africarxiv @africaosh

- Justin Ahinon, AfricArxiv.org/@justinahinon1

- Godfred Akwetey Brown /Ghana/ innovategh.com/facebook.com/godfred.brown

- Andrew quitmeyer @hikinghack www.dinacon.org

- Alexis C Johnson  https://twitter.com/pineapple_lab  (Twitter)    http://www.pineapplelaboratories.org/


## OPENING QUESTION: what brought you here, to this community call?  (~5')

## 1- SOIL CROMATOGRAPHY with @nanocastro (~15')
- Qualitative method to evaluate soil quality
- From biodynamics, Jairo Restrepo is the reference
- self evaluation in your soil, do it, perform some action, do it again

- Andre: way to sistematize this findings? / Nano: Greg Austic is working on correlations between soil parameters and crops

- rough description of method: digest sample of the soil with NaOH + paper filter + silver nitrate, different components of the soil solution migrate according to their different retention times

https://forum.openhardware.science/t/day-4-soil-chromatography-workshop/1512
https://docs.google.com/document/d/14UFbi4lz_3RSQtReoQWVZi5M56RRiuhEjwCt5ceF2vg/edit

- Andre: OpenMV
- Hey Nano, check out the OpenML Platform for Open and Collaborative machine Learning. You can generate a model and then save it to a special (90$ or so) USB stick (on a rasperri pi) that can process new images with this model and make evalutations based on your model (e.g. https://medium.com/deep-learning-turkey/a-brief-guide-to-intel-movidius-neural-compute-stick-with-raspberry-pi-3-f60bf7683d40, but there is at least another). // Cool thanks...

## 2- LEARNING SOMETHING NEW: CALIBRATION  with @julianstirling (~15')

* if we want to make measurements, we have to agree on e.g. what 1kg means.
* Historically we'd often use body geometry as a sort of "standard" - an inch is the top part of your thumb, a fathom is set by the action of pulling on a rope.  However, everybody's thumb is slightly different, so this isn't a great way to calibrate.
* 2-300 years ago, we started making artefacts to calibrate - e.g. a platinum rod 1m long, a platinum cylinder that weights exactly 1kg, both of which sit in a vault in Paris.
* Key units: Metre (length), Second (time), Kelvin (temperature), Ampere (current), Kilogram (mass)   [also there are two more Mole and Candela which are mostly still there for historical reasons]
* If we want to make sure our standards are the same, we have to send those artefacts around the world - e.g. I send my 1kg mass to the national lab, every so often the national lab send their 1kg mass to Paris, and so we establish a "chain" of measurements going back to the master copy.  This is difficult to manage and relies on the master artefact, it's fundamentally centralised.
* The new definition does away with this, so we can now check against fundamental Physics without needing to refer to the artefacts.
* The first definition was the second; this used to be defined relative to the rotation of the earth, but the earth's rotation isn't actually that constant!  Now, we use the frequency of an atomic transition; this is fixed by nature, and so we can use that frequency (which should be the same for everybody) instead of an artefact.  Atomic clocks are expensive, and we can't all make one - but there are a lot of atomic clocks including ones on GPS satellites, and so GPS time is referenced back to an atomic clock.
* The next one is the metre; this used to be defined by a stick of metal in Paris.  Now, it's defined by the speed of light - this is set as a constant.  That means we can use the wavelength of light (in an interferometer, which counts the waves of light as you move along a distance), together with our atomic clock (or GPS clock) to define a metre without referring back to Paris.
* We can link current and force using the force between two current-carrying wires.
* There are other constants that are now defined - e.g. Boltzmann's constant, ... which mean that it is now (in theory) possible to link all the fundamental quantities back to experiments rather than artefacts, and properly decentralise things.
* These measurements are, however, quite hard!  For now it is often still good to do comparisons with known standards, and we'd still need to do this in the future to check our experiments all agree, but in 20-30 years perhaps we'll be able to build open hardware to make these measurements and reduce reliance on standards labs.
  
- From HH: Ryan North just came out with a super fun book called "how to invent everything" that was just talking about recreating these units too! Reccomended!

- Q = One of the issues with community monitoring is that we always get attacked through the 'legitimacy' of non-compliance with standards, would this allow us to be 'legitimate in other way?    
Take a look at this paper https://journals.sagepub.com/doi/full/10.1177/1461444818777473 Re-Calibrating DIY.....
There's legitimacy in terms of certification, but also in process.  In some cases, there's a government specification (or even international standard) to comply with - e.g. for trade.  However, for science it's often more important to document your calibration process and explain the checks you've done and how you analyse your uncertainties.  Legal metrology for selling things or medical use is a bit different, and it's not certain how we could comply with that in an entirely decentralised way.

Julian is very keen to write up the document on calibration for GOSH (and possibly JOH) but this is of course hard to do both well and quickly!

- Q = Isn't encouraging people to calibrate their hardware (even to an artefact, or in a less-than-fully-traceable way) a much more immediate issue than global calibration standards...?    
Absolutely; Julian's intending to write about both of these things

## 3- NEXT CALL TIME, QUESTIONS, COMMENTS (5')
What time should next call happen? Leave here or contact Julieta

## 4- AFRICA OSH from Godfred
 http://africaosh.com/ Summit comming up on April 10-14 in Dar es Salaam, Tanzania:
- leads to additional potential sponsors?
- please spread the word and make some noise :) https://www.facebook.com/AfricaOSH/ / https://twitter.com/AfricaOSH / https://www.instagram.com/africaosh/

## 5- REPORT GOSH activities, help completing!
Andre: Jenny is collecting activities related to GOSH that was performed recently, please help fill it out! It helps with sourcing funding and sponsors for the network! https://forum.openhardware.science/t/collecting-gosh-talks-presentations-blogs-articles-etc/1723

