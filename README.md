# GOSH Roadmap 2017

This repository contains the GOSH Roadmap document in various 
formats, including its web assets for distribution and use.

Visit the [GOSH Roadmap website for more information](http://openhardware.science/global-open-science-hardware-roadmap).

## How to contribute

The complete text for the Roadmap is in a UTF-8 raw text file, 
formatted to 80 columns to facilitate accepting and tracking
contributions.

You can clone and use portions of this text in whatever publication
you like, but keep the license information below when doing so. 

If you want to propose changes, you can do it using 'git', and we will
merge all the contributions in a new file (containing all the changes
to the document that were submitted after the publication of the 
roadmap).

## Design files

It is included in the GOSH site all of the original files prepared by the 
designer, Maria del Lamadrid, as well as in open formats: EPS and SVG.
All the graphics and web assets are included well. Visit: http://openhardware.science/media

## Previous versions

We also included two major revisions of the document of February and 
April, 2017. They were exported to PDF and ODT. We also included two
major versions of the "scratch pads" we used to prepare each section
of the roadmap.

## License

The roadmap document was prepared by the GOSH community in 2017.
It is licensed under Creative Commons, CC-BY-4.00-International. The design 
files and web assets are being distributed under the same license.
